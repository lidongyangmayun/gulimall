package com.atldy.gulimall.coupon.dao;

import com.atldy.gulimall.coupon.entity.HomeSubjectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 专题商品
 * 
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 21:37:26
 */
@Mapper
public interface HomeSubjectSpuDao extends BaseMapper<HomeSubjectSpuEntity> {
	
}
