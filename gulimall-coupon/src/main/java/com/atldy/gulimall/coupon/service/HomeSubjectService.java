package com.atldy.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atldy.common.utils.PageUtils;
import com.atldy.gulimall.coupon.entity.HomeSubjectEntity;

import java.util.Map;

/**
 * 首页专题表【jd首页下面很多专题，每个专题链接新的页面，展示专题商品信息】
 *
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 21:37:26
 */
public interface HomeSubjectService extends IService<HomeSubjectEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

