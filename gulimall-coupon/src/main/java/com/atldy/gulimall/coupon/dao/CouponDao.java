package com.atldy.gulimall.coupon.dao;

import com.atldy.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 21:37:26
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
