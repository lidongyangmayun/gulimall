package com.atldy.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atldy.common.utils.PageUtils;
import com.atldy.gulimall.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 21:37:26
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

