package com.atldy.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atldy.common.utils.PageUtils;
import com.atldy.gulimall.coupon.entity.SeckillPromotionEntity;

import java.util.Map;

/**
 * 秒杀活动
 *
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 21:37:26
 */
public interface SeckillPromotionService extends IService<SeckillPromotionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

