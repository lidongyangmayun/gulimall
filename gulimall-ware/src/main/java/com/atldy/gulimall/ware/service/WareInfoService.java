package com.atldy.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atldy.common.utils.PageUtils;
import com.atldy.gulimall.ware.entity.WareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 15:04:50
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

