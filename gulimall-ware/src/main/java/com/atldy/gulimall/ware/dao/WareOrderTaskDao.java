package com.atldy.gulimall.ware.dao;

import com.atldy.gulimall.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 15:04:50
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
