package com.atldy.gulimall.product.service;

import com.atldy.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atldy.common.utils.PageUtils;

import java.util.Map;

/**
 * 商品三级分类
 *
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 09:59:53
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

