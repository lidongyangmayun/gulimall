package com.atldy.gulimall.product.service;

import com.atldy.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atldy.common.utils.PageUtils;

import java.util.Map;

/**
 * 品牌
 *
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 09:59:53
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

