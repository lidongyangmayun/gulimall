package com.atldy.gulimall.product.dao;

import com.atldy.gulimall.product.entity.ProductAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu属性值
 * 
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 09:59:53
 */
@Mapper
public interface ProductAttrValueDao extends BaseMapper<ProductAttrValueEntity> {
	
}
