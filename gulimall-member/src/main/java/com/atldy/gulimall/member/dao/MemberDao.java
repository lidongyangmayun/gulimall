package com.atldy.gulimall.member.dao;

import com.atldy.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 15:00:30
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
