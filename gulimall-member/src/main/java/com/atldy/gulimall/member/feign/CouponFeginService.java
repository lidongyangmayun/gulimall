package com.atldy.gulimall.member.feign;

import com.atldy.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @version 1.0
 * @Author: 唐多山
 * @CreateTime: 2021-02-09 21:44
 * @Description：
 */
@FeignClient("gulimall-coupon")
@Component
public interface CouponFeginService {

    @GetMapping("/coupon/coupon/member/list")
    public R memberList();

}