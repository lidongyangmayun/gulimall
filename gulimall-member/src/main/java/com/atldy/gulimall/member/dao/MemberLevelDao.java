package com.atldy.gulimall.member.dao;

import com.atldy.gulimall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 15:00:30
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {
	
}
