package com.atldy.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atldy.common.utils.PageUtils;
import com.atldy.gulimall.member.entity.MemberLoginLogEntity;

import java.util.Map;

/**
 * 会员登录记录
 *
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 15:00:30
 */
public interface MemberLoginLogService extends IService<MemberLoginLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

