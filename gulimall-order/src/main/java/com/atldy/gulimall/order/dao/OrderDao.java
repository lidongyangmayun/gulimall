package com.atldy.gulimall.order.dao;

import com.atldy.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author tangduoling
 * @email tangduoling@qq.com
 * @date 2021-02-09 14:55:02
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
